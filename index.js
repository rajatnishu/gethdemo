const express = require('express');
var app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
var cors = require('cors');
var request=require('request');
var fs = require("fs")
var config = require('./src/config/service')    
let port = process.env.PORT || 3000;
app.use(cors());
var Web3 = require('web3');
var web3 = new Web3();
var utils = require('web3-utils');
web3.setProvider(new web3.providers.HttpProvider('http://localhost:8545'));


var url = config.databaseurl
// var url = "mongodb://localhost:27017/demokixto";
mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }).then(function () {
  console.log("Connect Datebase");
}, function (err) {
  console.log(err)
})
// rpcapi="db,eth,net,web3,personal,web3,toHex"


// how to generate private key and address using ethereum 


// var Wallet = require('ethereumjs-wallet');
// let myWallet = new EthereumWallet()
// console.log("address: " + myWallet.getAddressString());
// console.log("privateKey: " + myWallet.getPrivateKeyString());


// Will convert an upper or lowercase Ethereum address to a checksum address.

// web3.utils.toChecksumAddress('0x9c34035e3E7Ab857103D88A47b582456Bf5cF417')
// .then(console.log)

// Checks the checksum of a given address. Will also return false on non-checksum addresses.

// web3.utils.checkAddressChecksum('0x9c34035e3E7Ab857103D88A47b582456Bf5cF417')
// .then(console.log)


// isaddress validation 
// web3.utils.isAddress('0x9c34035e3E7Ab857103D88A47b582456Bf5cF417')
// .then(console.log)

// create new accounts 

// var Account = web3.eth.accounts.create('oodles');
// console.log(Account)



// get all accounts 

// web3.eth.personal.getAccounts()
// .then(console.log);

// get unlockAccount  api 

// web3.eth.personal.unlockAccount("0x630B86ec52724F5c1d80e87c068e7bfB6C789ffB", "oodles", 600)
// .then(console.log('Account unlocked!'));


// sendTransaction api

// web3.eth.sendTransaction({
//   from: "0x630B86ec52724F5c1d80e87c068e7bfB6C789ffB",                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
//   gasPrice: "20000000000",
//   gas: "21000",                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
//   to: '0x9c34035e3E7Ab857103D88A47b582456Bf5cF417',
//   value: "10000",
//   data: ""
// }, 'oodles').then(console.log);


// // get allblock 

// web3.eth.getBlockNumber()      
// .then(console.log);

//  get all balance 
// web3.eth.getBalance('0x9c34035e3E7Ab857103D88A47b582456Bf5cF417')
// .then(console.log())

// get private key 

// web3.eth.accounts.privateKeyToAccount(privateKey [, ignoreLength ]);



// send transaction with private key in ethereum

// web3.eth.accounts.signTransaction({
//   to: '0x630B86ec52724F5c1d80e87c068e7bfB6C789ffB',
//   value: '1000000000',
//   gas: 2000000
// }, '0xa8e21cbb42b3f240767c0b73a940644c823771d7d9893b5ca6cb723e1e7d7be4')
// .then(console.log);


// Web3.eth.sendRawTransaction('0xf8668001831e848094630b86ec52724f5c1d80e87c068e7bfb6c789ffb843b9aca0080820a96a062700435a7f539f8a1575a2b3f6702e666b2b151195208d0979807033c1d503ea012c5ad8fe949db7e00a8e1254fdc7150795cd89741c36e31cacdd54d3b57b457')
// .then(console.log)

// get recoverTransaction through rawTransaction

// web3.eth.personal.ecRecover("0xf8668001831e848094630b86ec52724f5c1d80e87c068e7bfb6c789ffb843b9aca0080820a96a062700435a7f539f8a1575a2b3f6702e666b2b151195208d0979807033c1d503ea012c5ad8fe949db7e00a8e1254fdc7150795cd89741c36e31cacdd54d3b57b457")
// .then(console.log)


var adminapi = require('./src/Routes/admin_api');
const { generateKeyPair } = require('crypto');
app.get('/data', (req, res) => {
  res.send("api working fine")
})
app.listen(port, () => {
  console.log(`Example app is listening on port ${port}`);
})
app.use(bodyParser.json({ limit: '500mb' }));
app.use(bodyParser.urlencoded({ limit: '500mb', extended: true, parameterLimit: 50000000 }));
// app.use('/static', express.static('uploads/profile'));
app.use('/testapi', adminapi)
