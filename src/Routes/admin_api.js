var express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
var Web3 = require('web3');
var web3 = new Web3();
web3.setProvider(new web3.providers.HttpProvider('http://localhost:8545'));
rpcapi="db,eth,net,web3,personal,web3,toHex"

// create new account

router.get('/createaccount', async function (req, res) {
    let newaccount = await web3.eth.personal.newAccount('oodles', function(error, account){
        if (error) {
            res.send({ status: 200, data: error.message})
        } else{
            res.send({ status: 200, data: account })

        }
    });
    
})

// get all account list

router.get('/getaccountlist', async function (req, res) {
    var accountlist = await web3.eth.personal.getAccounts( function(error, accountlist){
   if(error){
    res.send({ status: 400, data: error.message  })
   } else{
    res.send({ status: 200, data: accountlist, length : accountlist.length})
   }
    })
})

// get accounts blance 

router.post('/getaccount', async function(req,res){
    var balance = await   web3.eth.getBalance(req.body.address)
    if(balance){
        res.send({ status: 200, data: balance, text: "get personal account balance!" })
    } else{
        res.send({ status: 400, data: balance, text: "something went worng!" })
    }
})


// get all blocknumber 

router.get('/allblock',async function(req,res){
   var block =  await web3.eth.getBlockNumber()
   if(block){
 res.send({ status: 200, data: block, text: "get personal account data!" })
   }
})


// get unlocked account 

router.post('/unlockaccount', async function(req,res){
    let account =  await web3.eth.personal.unlockAccount(req.body.address, req.body.password,15000)
    if(account){
        res.send({ status: 200, data:account, text: "Account unlocked!" })
    }
} )

// get locked account 

router.post('/lockaccount', async function(req,res){
    let account =  web3.eth.personal.lockAccount(req.body.address, req.body.password)
    if(account){
        res.send({ status: 200, data: account, text: "Account locked!" })
    }
} )

//  get a perticular block data 

router.post('/getblock', async function(req,res){
let block = web3.eth.getBlock(req.body.address, function(error,value){
   if(error){
res.send({status : 400 , data: error.message})
   } else{
    res.send({ status: 200, data: value }) 
   }
})
})

// send transaction through address by using call back function



router.post('/sendtransaction', async function(req,res){
let getvalue =  await web3.eth.sendTransaction({
      from: req.body.sender,
      to: req.body.reciver,
      gasPrice : req.body.gasPrice,
      gas: req.body.gas,
      value: req.body.value,
    }, function(error,hash){
        if(error){
      res.send({status :400, data : error.message })
        } else{
        res.send({ status: 200, data: hash }) 
        }
    })
})


// send transaction through address by using Promise 

router.post('/promisetrnasaction', async function(req,res){
   await web3.eth.sendTransaction({
    from: req.body.sender,
    to: req.body.reciver,
    value: req.body.value,
    })
    .then(function(receipt){
        res.send({status : 200 , data : receipt})
    });
})

// send transaction through address by using event emitter

router.post('/eventtrnasaction', async function(req,res){
    await web3.eth.sendTransaction({
     from: req.body.sender,
     to: req.body.reciver,
     value: req.body.value,
     })
    //  .on('receipt', function(receipt)
    //  .on('confirmation', function(confirmationNumber, receipt)  
    
    .on('confirmation', function(confNumber, receipt, latestBlockHash)

     {
        logconformation(confNumber, receipt.transactionHash)
     } ); 
     res.send({status : 200 })

 })

 function logconformation(conf, txhash){
console.log(conf, txhash)
 }



// sign transaction with private key 

router.post('/signtransaction', async function(req,res){
    let signdata =  await web3.eth.accounts.signTransaction({
          to: req.body.reciver,
          value: req.body.value,
          gas: req.body.gas
        }, req.body.privatekey)
        let transaction =  await web3.eth.sendSignedTransaction(signdata.rawTransaction, function(error,value){
            if(error){
                res.send({ status: 400, data: signdata , rec: error.message}) 
            } else{
                res.send({ status: 200, data: signdata , rec: value}) 
            }
        })
   
})

// current block Number

router.get('/blocknumber', async function(req,res){
    let block =  await web3.eth.getBlockNumber(function(err, number){
        if(err){
            res.send({ status: 400,  rec: error.message}) 
        } else{
            res.send({status : 200, data : number})
        }
    })
})






module.exports = router
